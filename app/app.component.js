"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Team = (function () {
    function Team() {
    }
    return Team;
}());
exports.Team = Team;
var team = [
    { id: 1, name: 'Cody', description: 'Cody Craft, the man behind our network.  So far, he has applied himself to understanding the database connection to the webpage and vice versa.  He has provided multiple options for database control and is looking to implement them this Sprint.', strength: 'Interface designs, troubleshooting, and programming. ', rest: 'Not Burger King' },
    { id: 2, name: 'Jared', description: 'Jared Caldwell, the project owner and envisionary of iFitness.  His help on steering the application to success is driven through his hard-working personality.', strength: 'Java and Software Development', rest: 'Chick-fil-A' },
    { id: 3, name: 'Joshua', description: 'Joshua Tatum, meet the website genius.  He has toileld around with website design for the application. Its interface is a friendly one, as well as interactive.  Imagine waht he could do in a year or two.,', strength: 'MySQL, HTML, CSS, and JAVA', rest: 'Anything sushi related' },
    { id: 4, name: 'Kipras', description: 'Kipras Stulgaitis, the project leader and communicator of the team.  With his leadership, iFitness team will continue down a path to greatness.  His skill in getting-to-the-point drives the meetings to fruition.  Cody, Jared, Joshua, and Sharif have great skills, and it is Kiprass job to make sure that htey are all on the same page.', strength: 'Programming, database, and ability to learn others quickly', rest: 'Shake Shack' },
    { id: 5, name: 'Sharif', description: 'Sharif Elkassed, divine database creator and debator.  He has come up with the database structure and will implement it for the future Sprint.  With the information that we had, his ERD is fit to be scallable.', strength: 'Programming, analyzing code, critical thinking, database design', rest: 'Burger King' }
];
var AppComponent = (function () {
    function AppComponent() {
        this.title = 'iFitness';
        this.team = team;
    }
    AppComponent.prototype.onSelect = function (team) {
        this.selectedTeam = team;
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "\n    <h1>{{title}}</h1>\n    <h2>My Team</h2>\n    <ul class=\"team\">\n      <li *ngFor=\"let team of team\"\n        [class.selected]=\"team === selectedTeam\"\n        (click)=\"onSelect(team)\">\n        <span class=\"badge\">{{team.id}}</span> {{team.name}}\n      </li>\n    </ul>\n    <div *ngIf=\"selectedTeam\">\n      <h2>{{selectedTeam.name}} details!</h2>\n      <div>{{selectedTeam.description}}</div>\n      <div><label><b>Strengths: </b></label>{{selectedTeam.strength}}</div>\n      <div><label><b>Favorite food: </b></label>{{selectedTeam.rest}}</div>\n    </div>\n  ",
            styles: ["\n    .selected {\n      background-color: #CFD8DC !important;\n      color: white;\n    }\n    .team {\n      margin: 0 0 2em 0;\n      list-style-type: none;\n      padding: 0;\n      width: 15em;\n    }\n    .team li {\n      cursor: pointer;\n      position: relative;\n      left: 0;\n      background-color: #EEE;\n      margin: .5em;\n      padding: .3em 0;\n      height: 1.6em;\n      border-radius: 4px;\n    }\n    .team li.selected:hover {\n      background-color: #BBD8DC !important;\n      color: white;\n    }\n    .team li:hover {\n      color: #607D8B;\n      background-color: #DDD;\n      left: .1em;\n    }\n    .team .text {\n      position: relative;\n      top: -3px;\n    }\n    .team .badge {\n      display: inline-block;\n      font-size: small;\n      color: white;\n      padding: 0.8em 0.7em 0 0.7em;\n      background-color: #607D8B;\n      line-height: 1em;\n      position: relative;\n      left: -1px;\n      top: -4px;\n      height: 1.8em;\n      margin-right: .8em;\n      border-radius: 4px 0 0 4px;\n    }\n  "]
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map