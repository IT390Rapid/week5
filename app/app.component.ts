import { Component } from '@angular/core';
export class Team {
  id: number;
  name: string;
  description: string;
  strength: string;
  rest: string;
}
const team: Team[] = [
  { id: 1, name: 'Cody' , description: 'Cody Craft, the man behind our network.  So far, he has applied himself to understanding the database connection to the webpage and vice versa.  He has provided multiple options for database control and is looking to implement them this Sprint.', strength: 'Interface designs, troubleshooting, and programming. ', rest: 'Not Burger King' },
  { id: 2, name: 'Jared' , description: 'Jared Caldwell, the project owner and envisionary of iFitness.  His help on steering the application to success is driven through his hard-working personality.', strength: 'Java and Software Development', rest: 'Chick-fil-A'},
  { id: 3, name: 'Joshua' , description: 'Joshua Tatum, meet the website genius.  He has toileld around with website design for the application. Its interface is a friendly one, as well as interactive.  Imagine waht he could do in a year or two.,' ,strength: 'MySQL, HTML, CSS, and JAVA', rest: 'Anything sushi related'},
  { id: 4, name: 'Kipras' , description: 'Kipras Stulgaitis, the project leader and communicator of the team.  With his leadership, iFitness team will continue down a path to greatness.  His skill in getting-to-the-point drives the meetings to fruition.  Cody, Jared, Joshua, and Sharif have great skills, and it is Kiprass job to make sure that htey are all on the same page.', strength: 'Programming, database, and ability to learn others quickly', rest: 'Shake Shack'},
  { id: 5, name: 'Sharif' , description: 'Sharif Elkassed, divine database creator and debator.  He has come up with the database structure and will implement it for the future Sprint.  With the information that we had, his ERD is fit to be scallable.', strength: 'Programming, analyzing code, critical thinking, database design', rest: 'Burger King'}
];
@Component({
  selector: 'my-app',
  template: `
    <h1>{{title}}</h1>
    <h2>My Team</h2>
    <ul class="team">
      <li *ngFor="let team of team"
        [class.selected]="team === selectedTeam"
        (click)="onSelect(team)">
        <span class="badge">{{team.id}}</span> {{team.name}}
      </li>
    </ul>
    <div *ngIf="selectedTeam">
      <h2>{{selectedTeam.name}} details!</h2>
      <div>{{selectedTeam.description}}</div>
      <div><label><b>Strengths: </b></label>{{selectedTeam.strength}}</div>
      <div><label><b>Favorite food: </b></label>{{selectedTeam.rest}}</div>
    </div>
  `,
  styles: [`
    .selected {
      background-color: #CFD8DC !important;
      color: white;
    }
    .team {
      margin: 0 0 2em 0;
      list-style-type: none;
      padding: 0;
      width: 15em;
    }
    .team li {
      cursor: pointer;
      position: relative;
      left: 0;
      background-color: #EEE;
      margin: .5em;
      padding: .3em 0;
      height: 1.6em;
      border-radius: 4px;
    }
    .team li.selected:hover {
      background-color: #BBD8DC !important;
      color: white;
    }
    .team li:hover {
      color: #607D8B;
      background-color: #DDD;
      left: .1em;
    }
    .team .text {
      position: relative;
      top: -3px;
    }
    .team .badge {
      display: inline-block;
      font-size: small;
      color: white;
      padding: 0.8em 0.7em 0 0.7em;
      background-color: #607D8B;
      line-height: 1em;
      position: relative;
      left: -1px;
      top: -4px;
      height: 1.8em;
      margin-right: .8em;
      border-radius: 4px 0 0 4px;
    }
  `]
})
export class AppComponent {
  title = 'iFitness';
  team = team;
  selectedTeam: Team;
  onSelect(team: Team): void {
    this.selectedTeam = team;
  }
}
